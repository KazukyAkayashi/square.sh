#!/bin/bash
## author : KazukyAkayashi
## git : https://codeberg.org/KazukyAkayashi/square.sh
## version : v2.02
## description : script to create a white outline for your images to create a square image.

## Bash strict mode ####################################
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

## Script ##############################################
##
## You need Imagemagick
##
########################################################

readonly SCRIPTNAME="$(basename "$0")"
echo "Start ${SCRIPTNAME}"

# If an argument is provided, use it as the working dir, otherwise the current dir will be used
if [[ -n "${1:-}" ]];
then
    cd "${1:-}"
fi

## Enter the desired name for the folder : web, pixelfed ...
FILE=web

if [ ! -d "$FILE" ];
then
    echo "Create ${FILE} directory"
    mkdir "${FILE}" || true
fi

## Imagemagick options ##################################
##
## Set your size with -extent
##
#########################################################

echo "Start Imagemagick"
for filename in *.jpg
do
    magick "${filename}" -resize 1080x1080\> -gravity center -extent 1180x1180 "$FILE"/"${filename%.jpg}"_web.jpg
    echo "$filename ✅"
done

echo "exiting ${SCRIPTNAME}"
exit 0

## END Script #####################################
