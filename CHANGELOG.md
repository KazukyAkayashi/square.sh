# Changelog

v2.02

- added a parameter that checks positioning to avoid errors.
- change `echo "$filename done"` for `echo "$filename ✅"`

v2.01

- fixed the problem of checking the output folder for images before placing them in the folder defined by the `$1` variable.

v2.00

- total script rewrite, use `gravity` and `extent` option, it's easier.
- you can now run the script from anywhere, just specify the path containing the images.