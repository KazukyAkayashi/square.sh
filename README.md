## Square.sh

Script to create a white outline for your images to create a square image.

For documentation : [Wiki Square script (Fr)](https://wiki.zarchbox.fr/photographie/script-bash/square-sh.html)

## Requirements :
- Imagemagick

## Use
Add an alias to your `.bashrc` or `.zshrc` ...

```bash
# Square
alias square="$HOME/your_path/square.sh"
```

 Once done, save and close the file. Make the aliases available in your current session by typing:

```
source ~/.bash.rc
```
And run the script

```
square your_path/your_pictures_folder
```
